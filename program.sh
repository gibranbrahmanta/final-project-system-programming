#! /bin/bash/

function pause() {
    local message="Press [Enter] key to continue..."
    echo -n $message
    read enter_key
}

function main_menu(){
    local pos=19
    echo "=============================="
    printf "%*s\n" $pos "MAIN MENU"
    echo "=============================="
    echo "1. Mount Flashdisk"
    echo "2. Unmount Flashdisk"
    echo "3. Eject Flashdisk"
    echo "4. Flashdisk Info"
    echo "5. Exit"
    echo -n "Choose 1-5: "
}

function header() {
    local message=$1
    local pos=$2
    echo ""
    echo ""
    echo "--------------------------------------------------"
    printf "%*s\n" $pos "$message"
    echo "--------------------------------------------------"
}

function ask_input() {
    read user_input
}

function generate_mount_dir() {
    mount_dir=$(fdisk -l | grep FAT32 | awk 'NR==1{print $1}')
    mount_dir_location="usb-mounting-folder"
}

function mount_flashdisk() {
    if [ -z "$mount_dir" ]
    then
        echo "Flashdisk not connected"
        pause
        return 0
    fi

    if [ ! -d "$mount_dir_location" ]
    then
        mkdir "$mount_dir_location"
    fi

    mount "$mount_dir" "$mount_dir_location"
    
    echo "Flashdisk mounted successfully"
    pause
}

function unmount_flashdisk() {
    if [ -z "$mount_dir" ]
    then
        echo "Flashdisk not mounted"
        pause
        return 0 
    fi
    umount "$mount_dir"

    echo "Flashdisk unounted successfully"
    pause
}

function eject_flashdisk() {
    eject "$mount_dir"

    echo "Flashdisk ejected successfully"
    pause
}

function flashdisk_info_menu(){
    local pos=22
    echo "=============================="
    printf "%*s\n" $pos "FLASHDISK INFO"
    echo "=============================="
    echo "1. Folder Hierarcy"
    echo "2. Flashdisk Size"
    echo "3. Exit"
    echo -n "Choose 1-3: "
}

function flashdisk_info() {
    if [ -z "$mount_dir" ]
    then
        echo "Flashdisk not found"
        pause
        return 0
    elif [ -z $(mount | grep "$mount_dir") ]
    then
        echo "Flashdisk not mounted"
        pause
        return 0
    fi

    while true
    do
        flashdisk_info_menu
        ask_input

        local re='^[0-9]+$'
        if [ -z $user_input ] || ! [[ $user_input =~ $re ]]
        then
            echo "Command Not Found"
        elif [ $user_input -eq 1 ]
        then
            folder_hierarchy
        elif [ $user_input -eq 2 ]
        then
            flashdisk_size
        elif [ $user_input -eq 3 ]
        then
            echo "Back to Main Menu..."
            pause
            break
        else
            echo "Command Not Found"
        fi
    done
}

function folder_hierarchy() {
    header "FOLDER HIERARCHY" 28

    echo "INITIAL DIRECTORY = $mount_dir_location"
    find "$mount_dir_location" | sed -e "s/[^-_][^\/]*\// |/g;s/|\([^ ]\)/|>>\1/"
    count=$(find $mount_dir_location -type d | wc -l)
    count=$(($count - 1))
    echo "TOTAL DIRECTORIES = $count"
    pause
}

function flashdisk_size() {
    total_size=$(fdisk -l | grep "$mount_dir" | awk '{print $6}')
    used_size=$(du -sh "$mount_dir_location" | awk '{print $1}')

    echo "Total disk size: ${total_size}B"
    echo "Used size: ${used_size}B"
    pause
}

function do_task() {
    local re='^[0-9]+$'
    if [ -z $user_input ] || ! [[ $user_input =~ $re ]]
    then
        echo "Command Not Found"
    elif [ $user_input -eq 1 ]
    then
        mount_flashdisk
    elif [ $user_input -eq 2 ]
    then
        unmount_flashdisk
    elif [ $user_input -eq 3 ]
    then
        eject_flashdisk
    elif [ $user_input -eq 4 ]
    then
        flashdisk_info
    elif [ $user_input -eq 5 ]
    then
        echo "Bye Bye..."
        pause
        exit 0
    else
        echo "Command Not Found"
    fi
    echo ""
    echo ""
}

# Generating mounting directory
generate_mount_dir

# Main program
while true
do
    main_menu
    ask_input
    do_task
done