# Final Project System Programming - Group "Julid"

## Tentang Sistem
* Kernel Version: 4.15.1

## Cara menggunakan script
1. Melakukan *clone* pada *repository* ini
2. Pastikan sudah melakukan instalasi *custom kernel*
	* Instalasi menggunakan *custom config* yang disediakan
	* dpkg -i linux-headers-4.15.1deb-julid-version_4.15.1deb-julid-version-10.00.Custom_amd64.deb
	* dpkg -i linux-image-4.15.1deb-julid-version_4.15.1deb-julid-version-10.00.Custom_amd64
3. Melakukan *copy* pada *file* berikut ke dalam *root folder* dari *user* (misalnya /home/user)
	* disable_service.sh
	* disable_service_list
	* install_driver.sh
	* program.sh
4. Pastikan anda berada di *root folder* dari *user*
5. Lakukan ``sudo disable_service.sh`` untuk men-*disable* pada *service* yang tidak digunakan
6. Lakukan ``sudo install_driver.sh`` untuk melakukan instalasi pada *driver* yang akan digunakan
7. Lakukan *reboot* pada sistem anda
8. Program siap digunakan

## Opsi pada Program
1. **Mount Flashdisk**: melakukan *mounting* pada *flashdisk* yang sudah disambungkan pada *port* USB
2. **Unmount Flashdisk**: melakukan *unmounting*
 pada *flashdisk* yang sudah disambungkan pada *port* USB
3.  **Eject Flashdisk**: melakukan *eject* pada *flashdisk* yang disambungkan pada *port* USB. Berguna jika ingin melakukan *safe remove* pada *flashdisk* agar tidak ada kerusakan data
4. **Flashdisk Info**: melihat informasi tentang *flashdisk*
	* **Folder Hierarcy**: melakukan print setiap *file* / *folder* yang ada di dalam *flashdisk* 
	* **Flashdisk Size**: melihat kapasitas total dari *flashdisk* dan berapa kapasitas yang sudah digunakan
	* **Exit**: kembali ke menu utama
5. **Exit**: keluar dari program