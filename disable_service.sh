#!/bin/bash

echo "Disable unused service for more faster userspace booting"
while read line
do
    systemctl disable $line
done < disable_service_list
echo "Finish disabling"